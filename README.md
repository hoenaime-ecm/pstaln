# PSTALN

## Installer les packages nécessaires

```bash
pip install -r requirements.txt
```

## Télécharger les données

```bash
wget -r --no-parent -nd -e robots=off -P data/ -A .json https://pageperso.lis-lab.fr/benoit.favre/covid19-data/20201206/
```

## Prétraiter des données

```bash
python3 src/preprocessing.py
```

## Organisation du dépôt
* `src` contient l'ensemble des script
* `data` contiendra les données téléchargées
* `ProjectManagement` contient les comptes rendus des réunions
* `modeles` contient les poids des réseaux de la deuxième approche