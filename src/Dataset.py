"""
Fichier contenant les classes permettant de manipuler les jeux de données
"""
# Imports de python
from typing import List
from pathlib import Path

# Imports de modules tiers
import pandas as pd
import sklearn
import seaborn as sns
import matplotlib.pyplot as plt

# Imports de scripts
from preprocessing import processing_X2tokens


class RealDataset:
    """
    Classe abstraite pour formaliser les classes pour intéragir avec les jeux de données
    """

    def __init__(self, path, name=''):
        """
        Constructeur

        :param str path: Chemin du json du jeux de données
        """
        self.path = path
        self.name = name

    def create_dataset(self) -> (pd.DataFrame, List[List[str]]):
        """
        Renvoie le couple X, Y du dataset brutes

        :return: Un dataframe X avec les colones 'title' et 'abstract' non préprocessé et une liste de liste de chaines
        de caractères correspondants aux topics des articles
        """
        raise NotImplementedError

    def create_and_fuse(self, other, shuffle=True, random_state=None) -> 'Dataset':
        """
        Crée de datasets et renvoie leur fusion

        :param RealDataset other: Dataset à fusionner avec celui_ci
        :param bool shuffle: Si True, mélange les jeux de données après la fusion
        :param int random_state: Graine pour la répétabilité
        :return: Les deux datasets fusionné et mélangé (si nécessaire) sous forme d'un dataset
        """
        ds1 = self.create_dataset()
        ds1.x["dataset"] = 0
        ds2 = other.create_dataset()
        ds2.x["dataset"] = 1
        return ds1.fuse(ds2, shuffle=shuffle, random_state=random_state)


class BLDataset(RealDataset):
    """
    Classe permettant de manipuler le jeu de données LitCovid et Bibliovid
    """

    def __init__(self, path, name=''):
        super().__init__(path, name)

    def create_dataset(self) -> 'Dataset':
        """
        Renvoie le couple X, Y du dataset brutes

        :return: Un dataframe X avec les colones 'title' et 'abstract' non préprocessé et une liste de liste de chaines
        de caractères correspondants aux topics des articles
        """
        df = pd.read_json(self.path)
        df_topics = df[~df["topics"].isna()]
        df_abstract = df_topics[~df_topics["abstract"].isna()]
        df_abstract_not_empty = df_abstract[df_abstract["abstract"] != '']
        df_work = df_abstract_not_empty[["title", "topics", "abstract"]]
        df_work.reset_index(inplace=True, drop=True)
        return Dataset(df_work[["title", "abstract"]], df_work.topics.to_list(), name=self.name)


class Dataset:
    """
    Classe pour manipuler un jeu de données
    """
    def __init__(self, x, y, name=''):
        self.x = x
        self.y = y
        self.name = name
        self.x_token = None

    def get_xy(self) -> (pd.DataFrame, List[List[str]]):
        """
        Retourne le X et le Y du dataset
        """
        return self.x, self.y

    def fuse(self, other, shuffle=True, random_state=None) -> 'Dataset':
        """
        Renvoie la fusion de deux dataset

        :param Dataset other: Dataset à fusionner avec celui_ci
        :param bool shuffle: Si True, mélange les jeux de données après la fusion
        :param int random_state: Graine pour la répétabilité
        :return: Les deux datasets fusionné et mélangé (si nécessaire) sous forme d'un dataset
        """
        x = pd.concat([self.x, other.x], ignore_index=True)
        y = self.y + other.y
        if shuffle:
            x, y = sklearn.utils.shuffle(x, y, random_state=random_state)
        return Dataset(x, y, name=f'{self.name} + {other.name}')

    def processing_x2tokens(self):
        """
        Crée la version tokénisée le jeu de données
        """
        if self.x_token is None:
            self.x_token = processing_X2tokens(self.x)

    def analyse(self):
        """
        Plot la répartition des longeurs des titres et abstract tokénisés
        """
        self.processing_x2tokens()
        fig_folder = Path('figures')
        fig_folder.mkdir(exist_ok=True)
        sns.displot(list(map(lambda x_token: len(x_token[0]), self.x_token)))
        title = f'Title - {self.name}'
        plt.title(title)
        plt.xlabel('Length')
        plt.savefig(fig_folder / title, bbox_inches='tight', pad_inches=0.1)
        plt.show()
        sns.displot(list(map(lambda x_token: len(x_token[1]), self.x_token)))
        title = f'Abstract - {self.name}'
        plt.xlabel('Length')
        plt.title(title)
        plt.savefig(fig_folder / title, bbox_inches='tight', pad_inches=0.1)
        plt.show()


if __name__ == '__main__':
    data_path = 'donnees'
    litcovid_path = Path(data_path) / 'litcovid.json'
    bibliovid_path = Path(data_path) / 'bibliovid.json'
    lit_dataset_interface = BLDataset(litcovid_path, 'Litcovid')
    bib_dataset_interface = BLDataset(bibliovid_path, 'Bibliovid')
    #lit_dataset = lit_dataset_interface.create_dataset()
    #bib_dataset = bib_dataset_interface.create_dataset()
    ds_fus = lit_dataset_interface.create_and_fuse(bib_dataset_interface, shuffle=True, random_state=0)
    x_fus, y_fus = ds_fus.get_xy()
    ds_fus.processing_x2tokens()
    x_token_fus = ds_fus.x_token
    ds_fus.analyse()
