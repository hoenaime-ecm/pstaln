
"""
@author: Mathias
"""

import numpy as np

import sklearn
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.estimator_checks import check_estimator
from sklearn.utils.validation import check_X_y, check_array, check_is_fitted, check_random_state
from sklearn.metrics import f1_score, accuracy_score, classification_report
from sklearn.preprocessing import MultiLabelBinarizer, LabelEncoder

import torch
from torch import optim, nn
from torch.utils.data import TensorDataset, DataLoader, random_split

from scipy.sparse import issparse

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class SKLearnWrapper(ClassifierMixin, BaseEstimator):
    def __init__(self, model_class, epochs=10, batch_size=32, multilabel=False, optimizer_class=optim.Adam, **kwargs):
        self.model_class = model_class
        self.kwargs = kwargs
        self.epochs = epochs
        self.batch_size = batch_size
        self.multilabel = multilabel
        self.optimizer_class = optimizer_class

    # training the model
    def fit(self, X, y):
        self.transformer = MultiLabelBinarizer() if self.multilabel else LabelEncoder()
        y = self.transformer.fit_transform(y)
        X, y = check_X_y(X, y, accept_sparse=True, multi_output=True) # check x and y

        input_size = X.shape[1]
        output_size = y.shape[1] if self.multilabel else y.max() + 1

        self.model = self.model_class(input_size, output_size, **self.kwargs)
        self.model.to(device)

        optimizer = self.optimizer_class(self.model.parameters())
        criterion = nn.MultiLabelSoftMarginLoss() if self.multilabel else nn.CrossEntropyLoss()

        X = self._to_torch(X)
        y = self._to_torch(y)
        
        dataset = TensorDataset(X, y)
        train_size = int(0.9 * len(dataset))
        train_set, val_set = random_split(dataset, [train_size, len(dataset) - train_size])
        train_loader = DataLoader(train_set, batch_size=self.batch_size, shuffle=True)
        val_loader = DataLoader(val_set, batch_size=self.batch_size, shuffle=True)

        for epoch in range(self.epochs):
            total_loss = num = 0
            self.model.train()
            for x, y in train_loader:
                x, y = x.to(device), y.to(device)
                optimizer.zero_grad()
                y_scores = self.model(x)
                loss = criterion(y_scores, y)
                loss.backward()
                optimizer.step()
                total_loss += loss.item()
                num += len(y)
            train_loss = total_loss / num

            total_loss = num = 0
            self.model.eval()
            for x, y in val_loader:
                with torch.no_grad():
                    x, y = x.to(device), y.to(device)
                    y_scores = self.model(x)
                    loss = criterion(y_scores, y)
                    total_loss += loss.item()
                    num += len(y)
            val_loss = total_loss / num

            print(epoch, train_loss, val_loss)

        self.is_fitted_ = True
        return self

    # predict values
    def predict(self, X, return_torch=False):
        check_is_fitted(self, 'is_fitted_') # check if model is fitted
        X = check_array(X, accept_sparse=True)
        X = self._to_torch(X).to(device)
        loader = DataLoader(TensorDataset(X), batch_size=self.batch_size)
        y = []
        self.model.eval()
        with torch.no_grad():
            for x in loader:
                x = torch.vstack(x)
                y.append(self.model(x).cpu())
        y = torch.vstack(y)
        return y if return_torch else y.numpy()

    @staticmethod
    def _to_torch(X):
        if issparse(X):
            Xcoo = X.tocoo()
            return torch.sparse.FloatTensor(torch.LongTensor([Xcoo.row.tolist(), Xcoo.col.tolist()]), torch.FloatTensor(Xcoo.data.astype(np.int32)), torch.Size(Xcoo.shape))
        return torch.from_numpy(X)

    def score(self, X, y):
        y = self.transformer.transform(y)
        if self.multilabel:
            y_pred = torch.sigmoid(self.predict(X, return_torch=True)).numpy() > 0.5
        else:
            y_pred = self.predict(X).argmax(axis=1)
        return classification_report(y, y_pred, target_names=self.transformer.classes_, zero_division=0)


if __name__ == '__main__':
    from sklearn.feature_extraction.text import CountVectorizer
    from sklearn.preprocessing import MultiLabelBinarizer
    from sklearn.model_selection import train_test_split
    import pandas as pd

    df = pd.read_json('data/litcovid.json')
    df = df[~df.title.isna() & ~df.topics.isna()]
    vectorizer = CountVectorizer()
    binarizer = MultiLabelBinarizer()
    
    X = vectorizer.fit_transform(df.title)
    y = binarizer.fit_transform(df.topics)

    X_train, X_test, y_train, y_test = train_test_split(X, y)

    class SimpleNN(nn.Module):
        def __init__(self, input_size, output_size):
            super().__init__()
            self.dense = nn.Linear(input_size, 100)
            self.decision = nn.Linear(100, output_size)
            
        def forward(self, x):
            return self.decision(self.dense(x))

    model = SKLearnWrapper(SimpleNN)
    #check_estimator(model)

    model.fit(X_train, y_train)
    print(model.score(X_test, y_test))