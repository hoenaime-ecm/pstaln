# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 21:16:25 2020

@authors: Clarence and Guillaume
"""
# Imports de modules tiers
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from Multi_VQ_VAE import VQ_VAE
from tqdm import tqdm

# Modèle à base de BERT (utilisation de l'attention)
# Import tokenizer for BERT
from transformers import BertTokenizer
tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')

from transformers import BertModel


class GRURepresentation(nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size, len_title):
        super().__init__()
        self.output_shape = 2*hidden_size
        self.embed = nn.Embedding(vocab_size, embed_size)
        self.title_gru = nn.GRU(embed_size, hidden_size, num_layers=1, bidirectional=False, batch_first=True)
        self.abstr_gru = nn.GRU(embed_size, hidden_size, num_layers=1, bidirectional=False, batch_first=True)
        self.title_dropout = nn.Dropout(0.3)
        self.abstr_dropout = nn.Dropout(0.3)
        self.len_title = len_title
    
    def forward(self, x_title, x_abstr):
        title_embed = self.embed(x_title)
        abstr_embed = self.embed(x_abstr)
        title_output, title_hidden = self.title_gru(title_embed)
        abstr_output, abstr_hidden = self.title_gru(abstr_embed)
        title_drop = self.title_dropout(title_hidden).squeeze(0)
        abstr_drop = self.abstr_dropout(abstr_hidden).squeeze(0)
        return torch.cat((title_drop, abstr_drop), -1)



class BaselineModel(nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size, nb_class, len_title):
        super().__init__()
        self.repr = GRURepresentation(vocab_size, embed_size, hidden_size, len_title)
        self.decision = nn.Linear(self.repr.output_shape, nb_class)

    def forward(self, x_title, x_abstr):
        cat = self.repr(x_title, x_abstr)
        return self.decision(cat.contiguous())

    def perf(self, loader):
        criterion = nn.BCEWithLogitsLoss()
        self.eval()
        total_loss = correct = num = 0
        for x, y in loader:
            with torch.no_grad():
                x_title = x[:, :self.repr.len_title]
                x_absract = x[:, self.repr.len_title:]
                y_scores = self(x_title, x_absract)
                loss = criterion(y_scores, y)
                y_pred = y_scores > 0.5
                correct += torch.sum(y_pred.data == y)
                total_loss += loss.item()
                num += len(y)
        return total_loss / num, correct.item() / num

    def fit(self, train_loader, valid_loader, epochs):
        criterion = nn.BCEWithLogitsLoss()
        optimizer = optim.Adam(self.parameters())
        for epoch in range(epochs):
            self.train()
            total_loss = num = 0
            for x, y in train_loader:
                optimizer.zero_grad()
                x_title = x[:, :self.repr.len_title]
                x_absract = x[:, self.repr.len_title:]
                y_scores = self(x_title, x_absract)
                loss = criterion(y_scores, y)
                loss.backward()
                optimizer.step()
                total_loss += loss.item()
                num += len(y)
            print(epoch, total_loss / num, *self.perf(valid_loader))

    def predict(self, loader):
        output = []
        for x, y in loader:
            with torch.no_grad():
                x_title = x[:, :self.repr.len_title]
                x_absract = x[:, self.repr.len_title:]
                y_scores = self(x_title, x_absract)
                y_pred = y_scores > 0.5
                output.append(y_pred.int())
        return output

# Definition classe modele à base de BERT + 1 couche dense de décision

class BertClassifier(nn.Module):
    def __init__(self, input_size, output_size, pad_token_id, state_dict=None, bert_freezed=False):
        super().__init__()
        self.pad_token_id = pad_token_id
        self.bert = BertModel.from_pretrained('bert-base-uncased', output_hidden_states = True) # Whether the model returns all hidden-states
        self.decision = nn.Linear(self.bert.config.hidden_size, output_size)

        if state_dict:
            print('loaded')
            self.bert.load_state_dict(state_dict)
            if bert_freezed:
                self.bert.requires_grad=False

    def forward(self, x): # fonction de prédiction 
        output = self.bert(x, attention_mask=(x != self.pad_token_id))
        return self.decision(torch.max(output[0], 1)[0])


class TwoWaysModel(nn.Module):
    def __init__(self, vocab_size, embed_size, hidden_size, nb_class_0, nb_class_1, len_title):
        super().__init__()
        self.repr = GRURepresentation(vocab_size, embed_size, hidden_size, len_title)
        self.decision_0 = nn.Linear(self.repr.output_shape, nb_class_0)
        self.decision_1 = nn.Linear(self.repr.output_shape, nb_class_1)
        self.nb_class_0 = nb_class_0

    def split_entry(self, x, y):
        """
        Découpe le batch en 2
        """
        x_title = x[:, :self.repr.len_title]
        x_absract = x[:, self.repr.len_title:-1]
        x_dataset = x[:, -1]
        x_0 = (x_title[x_dataset == 0], x_absract[x_dataset == 0], 0)
        x_1 = (x_title[x_dataset == 1], x_absract[x_dataset == 1], 1)
        y_0 = y[x_dataset == 0][:, :self.nb_class_0]
        y_1 = y[x_dataset == 1][:, self.nb_class_0:]
        return (x_0, y_0), (x_1, y_1)

    def forward(self, x_title, x_abstr, dataset_number):
        cat = self.repr(x_title, x_abstr)
        if dataset_number == 0:
            return self.decision_0(cat.contiguous())
        else:
            return self.decision_1(cat.contiguous())

    def perf(self, loader):
        criterion = nn.BCEWithLogitsLoss()
        self.eval()
        total_loss = [0, 0]
        correct = [0, 0]
        num = [0, 0]
        for x, y in loader:
            with torch.no_grad():
                for i, x_y_i in enumerate(self.split_entry(x, y)):
                    x_i, y_i = x_y_i
                    if len(y_i) > 0:
                        y_scores = self(*x_i)
                        loss = criterion(y_scores, y_i)
                        y_pred = y_scores > 0.5
                        correct[i] += torch.sum(y_pred.data == y_i)/len(y_i[0])
                        total_loss[i] += loss.item()
                        num[i] += len(y_i)
        num_0, num_1 = max(num[0], 1), max(num[1], 1)
        return total_loss[0] / num_0, correct[0].item() / num_0, total_loss[1] / num_1, correct[1].item() / num_1

    def fit(self, train_loader, valid_loader, epochs):
        criterion = nn.BCEWithLogitsLoss()
        optimizer = optim.Adam(self.parameters())
        for epoch in range(epochs):
            self.train()
            num = [0, 0]
            total_loss = [0, 0]
            for x, y in tqdm(train_loader, desc=f"Epoch {epoch}", ncols=75):
                losses = []
                weights = []
                x_y_list = self.split_entry(x, y)
                for x_y_i in x_y_list:
                    x_i, y_i = x_y_i
                    n = len(y_i)
                    weights.append(n)
                    if n > 0:
                        optimizer.zero_grad()
                        y_scores = self(*x_i)
                        losses.append(criterion(y_scores, y_i))
                    else:
                        losses.append(0)
                loss = (losses[0] * weights[0] + losses[1] * weights[1]) / sum(weights)
                loss.backward()
                optimizer.step()
                for i in range(len(weights)):
                    if losses[i] > 0:
                        total_loss[i] += loss.item()
                        num[i] += len(x_y_list[i][1])
            val_loss_0, val_loss_1, perf_0, perf_1 = self.perf(valid_loader)
            num_0, num_1 = max(num[0], 1), max(num[1], 1)
            print(" Dataset 0 - Loss :", total_loss[0] / num_0, ", Val_loss : ", val_loss_0, ", Perf : ", perf_0, "\n"
                  , "Dataset 1 - Loss :", total_loss[1] / num_1, ", Val_loss : ", val_loss_1, ", Perf : ", perf_1)

    def predict(self, loader):
        output_0, output_1 = [], []
        all_y_0, all_y_1 = [], []
        for x, y in loader:
            with torch.no_grad():
                (x_0, y_0), (x_1, y_1) = self.split_entry(x, y)
                y_scores_0, y_scores_1 = self(*x_0), self(*x_1)
                y_pred_0, y_pred_1 = y_scores_0 > 0.5, y_scores_1 > 0.5
                output_0.append(y_pred_0.int())
                output_1.append(y_pred_1.int())
                all_y_0.append(y_0)
                all_y_1.append(y_1)
        return (output_0, all_y_0), (output_1, all_y_1)



class VQModel(nn.Module):
    def __init__(self, CRModel, nb_labels, K, D, kernel_size=3):
        super().__init__()
        self.cr = CRModel
        self.vq_vae = VQ_VAE(self.cr.output_shape, nb_labels, K, D, kernel_size)
    
    def forward(self, x_title, x_abstr):
        with torch.no_grad():
            x = self.cr(x_title, x_abstr)
        return *self.vq_vae(x), x
    
    def perf(self, loader):
        self.eval()
        total_loss = num = 0
        for x, y in loader:
            with torch.no_grad():
                x_title = x[:, :self.cr.len_title]
                x_absract = x[:, self.cr.len_title:]
                
                vq_loss, x_recon, encd, x_c = self(x_title, x_absract)
                x_c = x_c.view(x_recon.shape)
                recon_loss = F.mse_loss(x_recon, x_c) # / data_variance
                total_loss += recon_loss.item()
                num += len(y)
        return total_loss / num
    
    def fit(self, train_loader, valid_loader, epochs):
        self.train()
        optimizer = optim.Adam(self.parameters(), lr=10**-4)
        for epoch in range(epochs):
            total_loss = num = 0
            for x, y in train_loader:
                optimizer.zero_grad()
                x_title = x[:, :self.cr.len_title]
                x_absract = x[:, self.cr.len_title:]
                
                vq_loss, x_recon, encd, x_c = self(x_title, x_absract)
                x_c = x_c.view(x_recon.shape)
                recon_loss = F.mse_loss(x_recon, x_c) # / data_variance
                loss = recon_loss + vq_loss
                loss.backward()
                
                optimizer.step()
                
                total_loss += loss.item()
                num += len(y)
            print(epoch, total_loss / num, self.perf(valid_loader))
    
    def predict(self, loader):  # TODO à refaire
        output = []
        for x, y in loader:
            with torch.no_grad():
                x_title = x[:, :self.cr.len_title]
                x_abstract = x[:, self.cr.len_title:]
                
                x = self.cr(x_title, x_abstract)
                output.append(self.vq_vae.predict(x))
        return output

if __name__ == '__main__':
    max_len = 16
    batch_size = 64
    embed_size = 128
    hidden_size = 128

    device = torch.device('cpu')  # can be changed to cuda if available
    
    rnn_model = BaselineModel(10,10,10,10)
    rnn_model.to(device)
