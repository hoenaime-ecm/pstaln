# -*- coding: utf-8 -*-
"""
Created on Sun Dec 27 09:29:45 2020

@author: clarence
"""
# Imports de modules tiers
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np

class VectorQuantizer(nn.Module):
    def __init__(self, K, D, beta=0.5):
        """
            K (int) : nombre d'embedding
            D (int) : dimension embedding
            beta (float) : cost of commitement loss
        """
        super().__init__()
        self.K = K
        self.D = D
        self.beta = beta
        
        self.embedding = nn.Embedding(self.K, self.D)
        self.embedding.weight.data.uniform_(-1/self.K, 1/self.K)
    
    def forward(self, inputs):
        if type(inputs) != torch.Tensor:
            inputs = torch.Tensor(inputs)
        input_shape = inputs.shape

        # Flatten input
        flat_input = inputs.view(-1, self.D)

        # Calculate distances
        distances = (torch.sum(flat_input**2, dim=1, keepdim=True) 
                    + torch.sum(self.embedding.weight**2, dim=1)
                    - 2 * torch.matmul(flat_input, self.embedding.weight.t()))

        encoding_indices = torch.argmin(distances, dim=1).unsqueeze(1)
        encodings = torch.zeros(encoding_indices.shape[0], self.K)
        encodings.scatter_(1, encoding_indices, 1)
        
        # Quantize and unflatten
        quantized = torch.matmul(encodings, self.embedding.weight).view(input_shape)

        # Loss
        e_latent_loss = F.mse_loss(quantized.detach(), inputs)
        q_latent_loss = F.mse_loss(quantized, inputs.detach())
        loss = q_latent_loss + self.beta * e_latent_loss

        quantized = inputs + (quantized - inputs).detach()
        
        return loss, quantized.contiguous(), encodings.view(input_shape[0], -1, self.K).sum(axis=1)

"""
class OldEncoder(nn.Module):
    def __init__(self, in_features, out_features, D, hidden=512):
        super().__init__()
        self.D = D
        self.hidden_size = hidden
        self.dense1 = nn.Linear(in_features, hidden)
        self.dense2 = nn.Linear(hidden, out_features)
    
    def forward(self, inputs):
        if type(inputs) != torch.Tensor:
            inputs = torch.Tensor(inputs)
        x = torch.flatten(inputs, start_dim=1)
        x = self.dense1(x)
        x = F.relu(x)
        x = self.dense2(x)
        x = F.relu(x)
        return x.view(inputs.shape[0], -1, self.D)


class OldDecoder(nn.Module):
    def __init__(self, out_features, shape, D, hidden=512):
        super().__init__()
        self.D = D
        self.shape = shape
        self.dense1 = nn.Linear(shape[0]*D, hidden)
        self.dense2 = nn.Linear(hidden, out_features)
    
    def forward(self, inputs):
        if type(inputs) != torch.Tensor:
            inputs = torch.Tensor(inputs)
        x = torch.flatten(inputs, start_dim=1)
        x = self.dense1(x)
        x = F.relu(x)
        x = self.dense2(x)
        x = F.relu(x)
        return x.view(inputs.shape[0], *self.shape)
"""

class Encoder(nn.Module):
    def __init__(self, input_shape, nb_labels, D, kernel_size=3):
        super().__init__()
        self.D = D
        self.dense = nn.Linear(input_shape, D + kernel_size-1)
        self.conv = nn.Conv1d(1, nb_labels, kernel_size)
        
    
    def forward(self, inputs):
        if type(inputs) != torch.Tensor:
            inputs = torch.Tensor(inputs)
        x = self.dense(inputs)
        x = F.relu(x)
        x = x.unsqueeze(1)
        x = self.conv(x)
        return x


class Decoder(nn.Module):
    def __init__(self, nb_labels, output_shape, D, kernel_size=3):
        super().__init__()
        self.D = D
        self.dense = nn.Linear(D - 1 + kernel_size, output_shape)
        self.conv = nn.ConvTranspose1d(nb_labels, 1, kernel_size)
    
    def forward(self, inputs):
        if type(inputs) != torch.Tensor:
            inputs = torch.Tensor(inputs)
        x = self.conv(inputs)
        x = x.squeeze(1)
        x = self.dense(x)
        return F.relu(x)
"""
class VQ_VAE(nn.Module):
    def __init__(self, input_shape, K, D):
        super().__init__()
        self.shape = input_shape
        in_features = input_shape[0]*input_shape[1]
        self.encoder = Encoder(in_features, input_shape[0]*D, D)
        self.vq = VectorQuantizer(K, D)
        self.decoder = Decoder(in_features, self.shape, D)

    def forward(self, x):
        ze = self.encoder(x)
        loss, zq, encodings = self.vq(ze)
        x_recon = self.decoder(zq)
        return loss, x_recon, encodings
    
    def predict(self, x):
        ze = self.encoder(x)
        loss, zq, encodings = self.vq(ze)
        return encodings
"""
class VQ_VAE(nn.Module):
    def __init__(self, input_shape, nb_labels, K, D, kernel_size=3):
        super().__init__()
        self.encoder = Encoder(input_shape, nb_labels, D, kernel_size)
        self.vq = VectorQuantizer(K, D)
        self.decoder = Decoder(nb_labels, input_shape, D, kernel_size)

    def forward(self, x):
        ze = self.encoder(x)
        loss, zq, encodings = self.vq(ze)
        x_recon = self.decoder(zq)
        return loss, x_recon, encodings

    def predict(self, x):
        ze = self.encoder(x)
        loss, zq, encodings = self.vq(ze)
        return encodings
    
    
if __name__ == '__main__':
    K = 5
    D = 3
    nb_labels = 4
    
    inp = np.zeros((2, 7))
    inp[0,5:] = 1
    inp[1,1:5] = 1
    print(inp)
    """
    enc = Encoder(inp.shape[1], nb_labels, D)
    encoded = enc(inp)
    print(encoded.shape)
    vq = VectorQuantizer(K, D)
    l, q, e = vq(encoded)
    print(q.shape)
    dec = Decoder(nb_labels, inp.shape[1], D)
    recon = dec(q)
    print(recon.shape)
    """
    vect = VQ_VAE(inp.shape[1], nb_labels, K, D)
    loss, x_recon, encodings = vect(inp)
    print(x_recon.shape)
    print(encodings.shape)