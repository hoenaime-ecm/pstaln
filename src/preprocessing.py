# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 13:47:42 2020

@authors: Clarence and Guillaume

Ce fichier contient les fonctions nécessaires au préprocessing des données
"""
# Imports de Python
import collections
from collections import Counter
from typing import List, Tuple, Union, Any

# Import de modules tiers
import pandas as pd
import numpy as np
import pickle
from utils import *
import nltk
from nltk.tokenize import word_tokenize
import torch
import torch.nn as nn

nltk.download('punkt')


def processing_y2cat(y, X_origin=None, get_inv_label=False) -> Union[np.ndarray, Tuple[np.ndarray, List[int]], Any]:
    """
    One hot encode y

    :param List[List[str]]|pd.Dataframe y: La liste des listes des topics
    :param List[int] X_origin: Liste des datasets d'origine chaque exemple
    :param bool get_inv_label: Si True, renvoie la liste des topics pour pouvoir les afficher ultérieurement
    :return La version one hot encodé de y
    """
    if type(y) == pd.DataFrame:
        y = y.to_list()
    seen_topics = []
    topic_origin = [[], []]
    for y_id, topics in enumerate(y):
        for topic in topics:
            if topic not in seen_topics:
                if X_origin is not None:
                    topic_origin[X_origin[y_id]].append(len(seen_topics))
                seen_topics.append(topic)
    seen_topics = np.array(seen_topics)

    n = seen_topics.shape[0]
    categories = np.empty((len(y), n))
    for i, topics in enumerate(y):
        cat = np.zeros((n,))
        for topic in topics:
            cat += np.where(seen_topics == topic, 1, 0)
        categories[i] = cat
    if X_origin is None:
        return categories
    else:
        new_categories = np.concatenate([categories[:, origin] for origin in topic_origin], 1)
        if get_inv_label:
            inv_label = np.concatenate((seen_topics[topic_origin[0]], seen_topics[topic_origin[1]]))
            return new_categories, [len(topic) for topic in topic_origin], inv_label
        else:
            return new_categories, [len(topic) for topic in topic_origin]


def processing_X2tokens(X) -> List[Tuple[List[str], List[str]]]:
    """
    Tokenise X à l'aide de nltk

    :param pd.Dataframe X: Dataframe contenant les colonnes title et abstract
    :return: La version tokenisée des titres et des abstracts
    """
    X_token = []
    titles = X.title.to_list()
    abstracts = X.abstract.to_list()
    for title, abst in zip(titles, abstracts):
        X_token.append((word_tokenize(title), word_tokenize(abst)))
    return X_token


def processing_tok2int(tokens) -> (List[Tuple[List[int], List[int]]], collections.defaultdict):
    """
    Transforme les tokens en entier pour pouvoir les passer dans un réseau

    :param List[(List[str], List[str])] tokens:
    :return: Les titres et abstracts sous forme de liste d'entier et le vocabulaire
    """
    vocab = collections.defaultdict(lambda: len(vocab))
    vocab['<eos>'] = 0

    int_X = []
    for title, abstr in tokens:
        int_X.append(([vocab[token] for token in title], [vocab[token] for token in abstr]))

    return int_X, vocab


def processing_max_length(int_X, title_max_len=30, abstr_max_len=500) -> torch.Tensor:
    """
    Tronque le jeu de données

    :param List[(List[int], List[int])] int_X: Les titres et abstract sous forme d'entier
    :param int title_max_len: La longueur maximale acceptée pour un titre
    :param int abstr_max_len: La longueur maximale acceptée pour un abstract
    :return: Les titres et les abstarct tronqués
    """
    X_title = torch.zeros(len(int_X), title_max_len).long()
    X_abstr = torch.zeros(len(int_X), abstr_max_len).long()

    for i, texts in enumerate(int_X):
        title, abstr = texts
        title_length = min(title_max_len, len(title))
        abstr_length = min(abstr_max_len, len(abstr))
        X_title[i, :title_length] = torch.LongTensor(title[:title_length])
        X_abstr[i, :abstr_length] = torch.LongTensor(abstr[:abstr_length])

    X = torch.cat((X_title, X_abstr), -1)
    return X

def corr_dict(cell, vocab):
    """
    How to use it : df.applymap(lambda x: corr_dict(x, vocab))
    :cell: a pandas cell in an applymap
    :vocab: the vocab you created previously with utils.get_vocab
    :return: np.array([list of ]), np.array([]) if empty
    """
    try:
        return np.vectorize(vocab.__getitem__)(cell)
    except:
        return np.array([])

def preprocessing(filename, vocab=None):
	"""
	Takes a json, reads it, creates a vocab dictionary with every word encountered.
	Matches every word with its label (~position encountered).
	Returns the cleaned dataframe (topics, title, abstract in lower case & lists of words instead of text)
	    and the vocab'd dataframe.

	:filename: string . The location of your json : "data/bibliovid.json"
	:vocab: dict . You might have a vocab to supply beforehand ?
	"""
	###preprocessing part (reusable elsewhere !)
	df = pd.read_json(filename)

    #list of the columns to extract the vocab
    col = ["title", "abstract","topics"]
    if "topics" not in df.columns:
    	df["topics"] = None

    df[col[:2]] = df[col[:2]].applymap(remove_punctuation)#remove punctuation (all except "-") & numerical
    df[col[:2]] = df[col[:2]].applymap(drop_non_utf8) #remove non-utf8 characters.
    df[col[-1]] = df[col[-1]].apply(none_to_list) #replace non string element of "topics" by an empty list
    df = df[col].applymap(lower) # lower the characters
    df[col[:2]] = df[col[:2]].applymap(string_split)#split phrases into words ("topics" columns is already)
    #what to do with numerical values ? remove them from dict
    #remove parenthesis and unwanted punctuation

    # We fetch the vocab from the whole dataframe.
    vocab = get_vocab(df, vocab=vocab)

    # We match every word with its label in a new vocab'd dataframe.
    df_vect = df.applymap(lambda x: corr_dict(x, vocab))

    return df, df_vect, vocab


# fonctions de récupération des données :

def get_titles(df, abstract=None): # renvoie la liste des titres du dataframe (bibliovid ou litcovid)
  try:
    df_topics = df[~df["topics"].isna()] # supprime du dataframe litcovid les lignes sans topic
    if abstract:
      df_abstract = df_topics[~df_topics["abstract"].isna()] # supprime du dataframe litcovid les lignes sans abstract
      titles = df_abstract["title"]
    else:
      titles = df_topics["title"]
  except KeyError:
    if abstract:
      df_abstract = df[~df["abstract"].isna()] # supprime du dataframe bibliovid les lignes sans abstract
      titles = df_abstract["title"]
    else:
      titles = df["title"]
  return titles.to_list()

def get_abstracts(df): # renvoie la liste des abstracts du dataframe (bibliovid ou litcovid)
  try:
    df_topics = df[~df["topics"].isna()] # supprime du dataframe litcovid les lignes sans topic
    df_abstract = df_topics[~df_topics["abstract"].isna()] # supprime du dataframe litcovid les lignes sans abstract
    abstracts = df_abstract["abstract"]
  except KeyError:
    df_abstract = df[~df["abstract"].isna()] # supprime du dataframe bibliovid les lignes sans abstract
    abstracts = df_abstract["abstract"]
  return abstracts.to_list()

def get_titles_and_abstracts(df): # renvoie la liste des titres et des abstracts du dataframe (bibliovid ou litcovid)
    titles = get_titles(df, abstract=True)
    abstracts = get_abstracts(df)
    both = [titles[i] + ". " + abstracts[i] for i in range(len(titles))]
    return both

def get_topics(df, abstract=None): # renvoie la liste des listes de topics du dataframe (bibliovid ou litcovid)
    df_topics = df[~df["topics"].isna()] # supprime du dataframe les lignes sans topic
    if abstract:
      df_abstract = df_topics[~df_topics["abstract"].isna()] # supprime du dataframe les lignes sans abstract
      topic_labels = df_abstract["topics"]
    else:
      topic_labels = df_topics["topics"]
    return topic_labels.to_list()

def get_category(df, abstract=None): # renvoie la liste des categories du dataframe de bibliovid
  if abstract:
    df = df[~df["abstract"].isna()] # supprime du dataframe les lignes sans abstract
  category = df["category"]
  category_names = []
  for row in category:
    category_names.append(row['name'])
  return category_names

def get_specialities(df, abstract=None): # renvoie la liste des listes des specialités du dataframe des données bibliovid et la liste des spécialités sans doublon
  if abstract:
    df = df[~df["abstract"].isna()] # supprime du dataframe les lignes sans abstract
  specialities = df["specialties"]
  specialities_names = []
  all = []
  for row in specialities:
    liste = []
    for element in row:
      liste.append(element['name'])
      all.append(element['name'])
    specialities_names.append(liste)
  return specialities_names, set(all)

def processing_dataset_origin(X) -> np.ndarray:
    """
    Renvoie un tableau contenant le numéro du jeu de données d'origine de chaque exemple

    :param pd.Dataframe X: Dataframe contenant la colonne dataset
    :return: Le tableau contenant le jeu de données d'origine de chaque exemple
    """
    X_origin = torch.zeros(X.shape[0], 1).int()
    unique_list = X.dataset.unique()
    dico_label = {}
    for elt_id, elt in enumerate(unique_list):
        dico_label[elt] = elt_id
    for elt_id, elt in enumerate(X.dataset):
        X_origin[elt_id] = dico_label[elt]
    return X_origin


def first_analysis(path) -> pd.DataFrame:
    """
    Réalise une première analyse d'un jeu de donnée

    :param str path: Chemin du json du jeu de données
    """
    #path = '../donnees/litcovid.json'
    #path = '../donnees/bibliovid.json'
    df = pd.read_json(path)
    print("\nDataset initial")
    df.info()

    df_topics = df[~df["topics"].isna()]
    print("\nTopics filtrés:")
    df_topics.info()

    df_abstract = df_topics[~df_topics["abstract"].isna()]
    df_abstract_not_empty = df_abstract[df_abstract["abstract"] != '']
    print("\nAbstracts filtrés:")
    df_abstract_not_empty.info()

    df_work = df_abstract_not_empty[["title", "topics", "abstract"]]

    topics = df_work.topics.to_list()
    print(df_work.topics)

    topic_count = Counter()
    nb_label_count = Counter()
    couple_count = Counter()

    for topic_list in topics:
        nb_label_count.update([len(topic_list)])
        for topic in topic_list:
            topic_count.update([topic])
        topic_list.sort()
        couple_count.update([tuple(topic_list)])

    print(nb_label_count)
    print(topic_count)
    print(couple_count)

    titles = df_work.title.to_list()
    titles_token = []
    for title in titles:
        titles_token.append(word_tokenize(title))
    return df_work


if __name__ == '__main__':

    # path = 'data/litcovid.json'
	vocab = None
    
	for f in ["data/bibliovid.json", "data/litcovid.json","data/cord19-metadata.json"]:#["data/bibliovid.json", "data/litcovid.json"]:

        #read data
		print("processing : "+f)

		df, df_vect, vocab= preprocessing(f, vocab=vocab)

		#save vectorized data in .feather file
		df_vect.to_feather(f[:-5] + ".feather")

		print(df.shape,   df_vect.shape)
		print(df.head(5), df_vect.head(5))
		# print(vocab)

	#save vocab dict in .vocab (outside the loop = we have a common vocabulary)
	pickle.dump(dict(vocab), open("data/vocab.pickle", "wb"))


		# try :
        #     df = pd.read_json(path)
        
        #     if "topics" not in df.columns:
        #         df["topics"] = "" # if not labeled, we create an empty label, to keep common preprocessing

        #     #lower the string data
        #     col_to_lower = ["topics","abstract","title"]
        #     df[col_to_lower] = df[col_to_lower].applymap(lower)

        #     #remove nan
        #     df_topics = df[~df["topics"].isna()]
        #     df_abstract = df_topics[~df_topics["abstract"].isna()]
        #     df_work = df_abstract[["title", "topics", "abstract"]]
        #     X = df_abstract[["title", "abstract"]]
        #     y = df_abstract["topics"]

        #     y_processed = processing_y2cat(y)
        #     #print(y_processed[:2])

        #     X_processed = processing_X2tokens(X)
        #     #print(X_processed[:1])

        #     X_processed, vocab = processing_tok2int(X_processed)
        #     #print(X_processed[:1])

        #     X_concat = processing_max_length(X_processed, 10, 15) # X_concat is the concatenation of X_title and X_abstract (resp. length are 10 and 15)
        #     #print(X_concat[:1])

        #     # save data in /data or /donnes directory
        #     torch.save(X_concat, open(path[:-5] + ".torch", "wb"))
        #     pickle.dump(dict(vocab), open(path[:-5]+".vocab", "wb"))
        #     y.to_pickle(open(path[:-5] + ".label","wb"))
            
        # except Exception as e:
        #     print(e)
        

