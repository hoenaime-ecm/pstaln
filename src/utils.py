# -*- coding: utf-8 -*-
"""
@author: klegoff
"""
import collections
import numpy as np
import pandas as pd
import langid


def reverse_dict(dict_):
	"""
	input : dict_ (type = dict)
	output : out_dict (type = dict)
	return dict with inverted values and keys
	/!\ needs unicity of values /!\
	"""
	out_dict = {}
	val = list(dict_.values())
	key = list(dict_.keys())
	for i in range(len(key)):
		out_dict[val[i]] = key[i]

	return out_dict

def lower(elem):
	"""
	lower string or return the unprocessed element
	works if string object are contained in a list
	to be applied to pandas object (ex : df.applymap(lower))
	"""
	try :
		return elem.lower()
	except :
		if type(elem)==list:
			try:
				return [x.lower() for x in elem]	
			except:
				return elem
		else:
			return elem
        
def string_split(string):
	"""
	return splitted string
	if exception, return unprocessed element
	"""
	try :
		return string.split()

	except Exception as e:
		#print(e)
		return string

def remove_punctuation(string, punctuation=['!', "?", ",", ":", ";", "(", ")", "[", "]", ". "]):
	"""
	remove listed punctuation from string list
	"""
	if type(string)==str:
		for punc in punctuation:
			string = string.replace(punc, " ") # replace any occurence of the punctation by spacing

		return string

	else:
		return ""

def none_to_list(elem):
	"""
	if not a list, return an empty list
	"""
	if type(elem) != list:
		return []
	else:
		return elem

def get_vocab(df, vocab=None):
	"""
	compute vocabulary dictionnary for a selected file name
	if we want to complete already existing vocab, pass it in parameters

	inputs:
	-df = dataframe we want to extract the vocab from (pd.DataFrame)
	-vocab = dict object (or Non if needs to be created in the function)
	"""
	if vocab == None:
		vocab = {}
		vocab['<eos>'] = 0

	###update the vocab
	if df.shape[0]>64000:
		###get vocab from our data, by 3000 element chunks (far less ressource-intensive)
		start_idx = [3000*i for i in range(len(df)//3000)]
		end_idx = start_idx[1:] + [-1]

		for i in range(len(start_idx)):
			#print(i)
			vocab = enrich_vocab(df.iloc[start_idx[i]:end_idx[i],:], vocab)
	else :
		vocab = enrich_vocab(df, vocab)

	return vocab

def enrich_vocab(df, vocab):
	"""
	add all the words in an iterable to the vocab dict
	"""
	word_set = set(np.concatenate(df.values.reshape(-1))) - set(vocab.keys()) # filter out already existing words in the vocabulary

	idx = len(vocab)
	for word in word_set:
		vocab[word] = idx
		idx +=1

	return vocab

def drop_non_utf8(string):
	"""
	Removes all characters that are not UTF-8 compliant.
	How to use it : pd.Series.apply(drop_non_utf8)
	1 mn over 374 000 rows in cord19 json.
	:string: a ... string !
	:return: string cleansed of non-utf8 characters
	"""
	try:
		return ''.join(i for i in string if ord(i)<128)
	except:
		return "" # if type problem, we return an empty string

def guess_language(df, column_name='abstract'):
	"""
	Guess the language of each row using the column_name.
	Relies on langid. (Not the fastest but quite reliable for english detection)
	Warning : Takes 9hrs/(number of virtual cores on your PC * GHz of your CPU)
	(That makes 20 minutes on a 8-core i7-5700hq 3.5 GHz)
	:df: pd.DataFrame 
	:column_name: string, must be in df.columns.
	:return: df with a new column : langid_ + column_name, containing the guessed language.
	"""
	return df[column_name].apply(drop_non_utf8).apply(langid.classify).apply(lambda x : tuple(x)[0])

if __name__ == '__main__':

	for f in ["data/bibliovid.json","data/litcovid.json"]:#["data/bibliovid.json","data/litcovid.json","data/cord19-metadata.json"]:
		print("processing : "+f)

		#read raw json data
		df = pd.read_json(f)

		#Get the indication of the languages of "title" and "abstract" columns 
		# /!\ Can slow down the preprocess a lot /!\
		df["title_lang"]= guess_language(df, "title")
		df["abstract_lang"] = guess_language(df, "abstract")

		#save into json
		df.to_json(f[:-5] + "-lang" + ".json") # files will be named *-lang.json


	ret_col_name = "langid_" + column_name

	return pd.concat((df, pd.DataFrame(langg_ser.rename(ret_col_name))), axis=1)
